package com.oxygenventures.amity.main;

import java.util.Random;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.oxygenventures.amity.driver.SeleniumDriver;
import com.oxygenventures.amity.objects.AmityCombination;
import com.oxygenventures.amity.objects.ExtendedRunner;
import com.oxygenventures.amity.objects.Repeat;
import com.oxygenventures.amity.tests.AmitySearchTest;
import com.oxygenventures.amity.tests.LaunchBrowserTest;
import com.oxygenventures.amity.tests.SearchedContentsTest;
import com.oxygenventures.amity.utils.Constants;
import com.oxygenventures.amity.utils.FileUtils;

@RunWith(ExtendedRunner.class)
public class ChromeSearchTest {

	WebDriver driver = null;
	AmityCombination combo;
	String result;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		FileUtils.createReport();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		driver = SeleniumDriver.setWebDriver(SeleniumDriver.CHROME_DRIVER);
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

//	@Test
	public void clickAllPropertyTypes() {
		FileUtils.createReport();
		new LaunchBrowserTest(driver).performAction();
		for (int property_type = 0; property_type < Constants.Selectors.AMITY_PROPERTY_TYPE_OPTIONS.length; property_type++) {
			for (int suburb = 1; suburb <= AmitySearchTest.getSuburbSize(driver); suburb++) {
				for (int beds = 0; beds < Constants.Selectors.AMITY_BEDS_OPTIONS.length; beds++) {
					for (int baths = 0; baths < Constants.Selectors.AMITY_BATHS_OPTIONS.length; baths++) {
						for (int carparks = 0; carparks < Constants.Selectors.AMITY_CARPARK_OPTIONS.length; carparks++) {
							combo = new AmitySearchTest(driver,
									Constants.Selectors.AMITY_PROPERTY_TYPE_OPTIONS[property_type],
									"#suburb > option:nth-child("+suburb+")",
									Constants.Selectors.AMITY_BEDS_OPTIONS[beds],
									Constants.Selectors.AMITY_BATHS_OPTIONS[baths],
									Constants.Selectors.AMITY_CARPARK_OPTIONS[carparks])
									.searchHomePage();
							
							SearchedContentsTest test = new SearchedContentsTest(driver, combo); 
							
							result = test.doTest();
							FileUtils.writeReport(combo, result, test.getListOfAssertions());
							
							combo.setPrice("0;0");
							test.getListOfAssertions().clear();
							
							result = test.doTest();
							FileUtils.writeReport(combo, result, test.getListOfAssertions());
							
							new LaunchBrowserTest(driver).performAction();
						}
					}
				}
			}
		}
	}
	
	@Test
	@Repeat(value=20)
	public void randomCombination() {
		new LaunchBrowserTest(driver).performAction();
		
		Random random = new Random();
		int property_type = random.nextInt(Constants.Selectors.AMITY_PROPERTY_TYPE_OPTIONS.length);
		int suburb = random.nextInt(AmitySearchTest.getSuburbSize(driver));
		int beds = random.nextInt(Constants.Selectors.AMITY_BEDS_OPTIONS.length);
		int baths = random.nextInt(Constants.Selectors.AMITY_BATHS_OPTIONS.length);
		int carparks = random.nextInt(Constants.Selectors.AMITY_CARPARK_OPTIONS.length);
		
		while (suburb == 0) {
			suburb = random.nextInt(AmitySearchTest.getSuburbSize(driver));
		}
		
		combo = new AmitySearchTest(driver,
				Constants.Selectors.AMITY_PROPERTY_TYPE_OPTIONS[property_type],
				"#suburb > option:nth-child("+suburb+")",
				Constants.Selectors.AMITY_BEDS_OPTIONS[beds],
				Constants.Selectors.AMITY_BATHS_OPTIONS[baths],
				Constants.Selectors.AMITY_CARPARK_OPTIONS[carparks])
				.searchHomePage();
		
		SearchedContentsTest test = new SearchedContentsTest(driver, combo); 
		
		result = test.doTest();
		FileUtils.writeReport(combo, result, test.getListOfAssertions());
		
		combo.setPrice("0;0");
		test.getListOfAssertions().clear();
		
		result = test.doTest();
		FileUtils.writeReport(combo, result, test.getListOfAssertions());
		
		new LaunchBrowserTest(driver).performAction();
	}

}
