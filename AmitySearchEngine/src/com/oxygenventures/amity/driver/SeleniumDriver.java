package com.oxygenventures.amity.driver;

import java.io.BufferedReader;
import java.io.FileReader;
import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Assert;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class SeleniumDriver {

	public static final int CHROME_DRIVER = 1;
	public static final int FIREFOX_DRIVER = 2;
	public static final int SAFARI_DRIVER = 3;
	public static final int IE9_DRIVER = 4;
	public static final int IE11_DRIVER = 5;
	public static final String WINDOWS_OS = "windows";
	public static final String MAC_OS = "mac";
	private static DesiredCapabilities capabilities;
	
//	public static final String CHROME_DRIVER_ADDRESS = "http://10.4.250.13:9515/wd/hub";
	public static final String CHROME_DRIVER_ADDRESS = "http://127.0.0.1:9515/wd/hub/";
	public static final String IE11_DRIVER_ADDRESS = "http://192.168.56.1:4444/wd/hub";
	

	static WebDriver driver;

	public static String getDriverPath(String driver) {
		String path = "";
		try (BufferedReader br = new BufferedReader(new FileReader("config"+System.getProperty("file.separator")+"config.txt"));){

			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			
			String everything = sb.toString();

			String[] lines = everything.split("\n");

			for (String linne : lines) {
				if (linne.contains(driver)) {
					path = linne.split("=")[1];
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return path;
	}

	public static String getOS() {
		String os = System.getProperty("os.name");
		
		if (os.toLowerCase().contains(WINDOWS_OS)) {
			os = WINDOWS_OS;
		}
		else if (os.toLowerCase().contains(MAC_OS)) {
			os = MAC_OS;
		}
		else {
			Assert.fail("Unknown OS: " + os);
		}
		
		return os;
	}
	
	
	public static WebDriver setWebDriver(int key) {
		String prefix = "";
		
		prefix = getOS();
		
		System.out.println(prefix);
		
		capabilities = new DesiredCapabilities();
		
		switch (key) {
		case CHROME_DRIVER:
			System.setProperty("webdriver.chrome.driver", getDriverPath(prefix + "_chrome_driver").replace("*", System.getProperty("file.separator")).trim());
			System.out.println(getDriverPath(prefix + "_chrome_driver").replace("*", System.getProperty("file.separator")).trim());
			ChromeOptions options = new ChromeOptions();
			options.addArguments("start-maximized");
			driver = new ChromeDriver(options);
			
			if (prefix.equals(MAC_OS)) {
				System.out.println("mac will be ready");
				try {
			        driver = new RemoteWebDriver(new URL(CHROME_DRIVER_ADDRESS), DesiredCapabilities.chrome());
			        System.out.println("driver initiated");
			      } catch (MalformedURLException e) {
			        e.printStackTrace();
			      } catch (Exception e) {
			    	  e.printStackTrace();
			    }
			}
			
			
			break;

		case FIREFOX_DRIVER:
			System.setProperty("webdriver.firefox.bin", getDriverPath(prefix + "_firefox_driver").replace("*", System.getProperty("file.separator")).trim());
			capabilities.setJavascriptEnabled(true);
			capabilities.setPlatform(Platform.WINDOWS);
			capabilities.setBrowserName(DesiredCapabilities.firefox().getBrowserName());
			capabilities.setVersion("24");
			driver = new FirefoxDriver(capabilities);
			driver.manage().window().maximize();
			break;
			
		case IE9_DRIVER:
		case IE11_DRIVER:
			System.setProperty("webdriver.ie.driver", getDriverPath(prefix + "_ie9_driver").replace("*", System.getProperty("file.separator")).trim());
			driver = new InternetExplorerDriver();
			driver.manage().window().maximize();
			break;
		}

		return driver;
	}
}
