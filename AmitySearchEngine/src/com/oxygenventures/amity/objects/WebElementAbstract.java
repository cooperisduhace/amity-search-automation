package com.oxygenventures.amity.objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public abstract class WebElementAbstract {
	WebElement element;
	public abstract boolean isElementNull();
	public abstract WebElement getElement(WebDriver driver, By by, int timeout);
}
