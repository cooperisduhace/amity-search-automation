package com.oxygenventures.amity.objects;

import org.openqa.selenium.WebDriver;

public abstract class Test {

	protected WebDriver driver;
	protected int browser;

	public Test(WebDriver driver) {
		this.driver = driver;
	}

	public Test(WebDriver driver, int browser) {
		this.driver = driver;
		this.browser = browser;
	}
	
	public abstract void performTest();

	public abstract void performAction();

}
