package com.oxygenventures.amity.objects;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface SmokeTest {

//	public boolean isEnabled() default true;
}
