package com.oxygenventures.amity.objects;

public class Result {
	private static final String PASS = "-- PASS";
	
	String name, beds, baths, carparks, price, finalresults;

	public String getFinalresults() {
		return finalresults;
	}

	public void setFinalresults(String finalresults) {
		this.finalresults = finalresults;
	}
	
	public void setFinalresultss(String name, String beds, String baths, String carparks, String price) {
		if (name.contains(PASS) && beds.contains(PASS) && baths.contains(PASS) && carparks.contains(PASS) && price.contains(PASS)) {
			this.finalresults = "PASS";
		} else {
			this.finalresults = "FAIL!" + "Name: "+ name + "Beds: "+ beds + "Baths: "+ baths + "Carparks: "+ carparks + "Price: "+ price;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBeds() {
		return beds;
	}

	public void setBeds(String beds) {
		this.beds = beds;
	}

	public String getBaths() {
		return baths;
	}

	public void setBaths(String baths) {
		this.baths = baths;
	}

	public String getCarparks() {
		return carparks;
	}

	public void setCarparks(String carparks) {
		this.carparks = carparks;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
}
