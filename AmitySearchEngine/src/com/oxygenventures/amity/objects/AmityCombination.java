package com.oxygenventures.amity.objects;

public class AmityCombination {
	
	String property_types, suburb, beds, baths, carparks, price;

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getPropertyTypes() {
		return property_types;
	}

	public void setPropertyTypes(String property_types) {
		this.property_types = property_types;
	}

	public String getSuburb() {
		return suburb;
	}

	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}

	public String getBeds() {
		return beds;
	}

	public void setBeds(String beds) {
		this.beds = beds;
	}

	public String getBaths() {
		return baths;
	}

	public void setBaths(String baths) {
		this.baths = baths;
	}

	public String getCarparks() {
		return carparks;
	}

	public void setCarparks(String carparks) {
		this.carparks = carparks;
	}
	
	

}
