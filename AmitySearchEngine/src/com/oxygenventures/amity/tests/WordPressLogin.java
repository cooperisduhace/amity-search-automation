package com.oxygenventures.amity.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.oxygenventures.amity.objects.Test;
import com.oxygenventures.amity.utils.Constants;
import com.oxygenventures.amity.utils.WebDriverUtils;

public class WordPressLogin extends Test {

	public WordPressLogin(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void performTest() {
	}

	@Override
	public void performAction() {
		new WebDriverWait(driver, Constants.SHORT_TIMEOUT)
				.until(ExpectedConditions.presenceOfElementLocated(By
						.cssSelector(Constants.Selectors.WORDPRESS_USERNAME_FIELD)))
				.sendKeys(Constants.WORDPRESS_USERNAME);

		new WebDriverWait(driver, Constants.SHORT_TIMEOUT)
				.until(ExpectedConditions.presenceOfElementLocated(By
						.cssSelector(Constants.Selectors.WORDPRESS_PASSWORD_FIELD)))
				.sendKeys(Constants.WORDPRESS_PASSWORD);

		WebElement login = new WebDriverWait(driver, Constants.SHORT_TIMEOUT)
				.until(ExpectedConditions.presenceOfElementLocated(By
						.cssSelector(Constants.Selectors.WORDPRESS_LOGIN_BUTTON)));
		WebDriverUtils.scrollToElement(driver, login, WebDriverUtils.BY_JAVASCRIPT);
		login.click();
	}

}
