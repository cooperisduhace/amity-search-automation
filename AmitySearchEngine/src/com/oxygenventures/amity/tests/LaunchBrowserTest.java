package com.oxygenventures.amity.tests;

import org.openqa.selenium.WebDriver;

import com.oxygenventures.amity.objects.Test;
import com.oxygenventures.amity.utils.Constants;

public class LaunchBrowserTest extends Test {

	public LaunchBrowserTest(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void performAction() {
//		driver.get("http://"+Constants.AMITY_USERNAME+":"+Constants.AMITY_PASSWORD+"@"+Constants.AMITY_URL);
		driver.get("http://"+Constants.AMITY_URL_NEW);
	}

	@Override
	public void performTest() {
	}

}
