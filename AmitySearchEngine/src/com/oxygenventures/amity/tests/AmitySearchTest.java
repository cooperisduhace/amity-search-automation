package com.oxygenventures.amity.tests;

import java.util.Random;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.oxygenventures.amity.objects.AmityCombination;
import com.oxygenventures.amity.objects.Test;
import com.oxygenventures.amity.utils.Constants;
import com.oxygenventures.amity.utils.WebDriverUtils;

@SuppressWarnings("deprecation")
public class AmitySearchTest extends Test {

	private String property_type, suburb, beds, baths, carparks;
	private static final int PROPERTY_TYPE = 1;
	private static final int SUBURB = 2;
	private static final int BEDS = 3;
	private static final int BATHS = 4;
	private static final int CARPARKS = 5;
	public AmityCombination combo;

	public AmitySearchTest(WebDriver driver, String property_type,
			String suburb, String beds, String baths, String carparks) {
		super(driver);
		this.property_type = property_type;
		this.suburb = suburb;
		this.beds = beds;
		this.baths = baths;
		this.carparks = carparks;
		System.out.println("Test Computation: Property Type: " + property_type
				+ ", Suburb: " + suburb + ", Beds: " + beds + ", Baths: "
				+ baths + ", Carparks: " + carparks);
	}

	@Override
	public void performTest() {
	}

	@Override
	public void performAction() {
	}

	public AmityCombination searchHomePage() {
		clickOptions();
		try {
			WebElement slider = WebDriverUtils.findElementByCssSelector(driver,
					Constants.Selectors.AMITY_PRICE_SLIDER);
	
			Random random = new Random();
			int min = WebDriverUtils
					.findElementByCssSelector(driver,
							Constants.Selectors.AMITY_PRICE_START).getLocation()
					.getX()
					- slider.getLocation().getX();
			int max = WebDriverUtils
					.findElementByCssSelector(driver,
							Constants.Selectors.AMITY_PRICE_END).getLocation()
					.getX()
					- slider.getLocation().getX();
			int rand = random.nextInt((max - min) + 1);
			System.out.println("Minimum value: "+String.valueOf(min));
			System.out.println("Maximum value: "+String.valueOf(max));
			System.out.println("Random value: "+String.valueOf(rand));
	
			WebDriverUtils.dragElementByOffset(driver, slider, rand, 0);
		} catch (TimeoutException e) {
			e.printStackTrace();
			Assert.fail("Slider couldn't be found");
		}
		
		combo.setPrice(WebDriverUtils.findElementByCssSelector(driver, Constants.Selectors.AMITY_PRICE_GETTER).getAttribute("value"));
		WebDriverUtils.findElementByCssSelector(driver,
				Constants.Selectors.AMITY_SEARCH_BUTTON).click();
		
		return combo;
	}

	public AmityCombination searchSearchedPage() {
		clickOptions();

		WebDriverUtils.findElementByCssSelector(driver,
				Constants.Selectors.AMITY_SEARCH_BUTTON).click();
		
		return combo;
	}
	
	public static int getSuburbSize(WebDriver driver) {
		WebElement suburboption = WebDriverUtils.findElementById(driver, "suburb");
		return suburboption.findElements(By.xpath(".//option")).size();
	}

	private void clickOptions() {
		WebElement iproperty_type = WebDriverUtils.findElementByCssSelector(
				driver, Constants.Selectors.AMITY_PROPERTY_TYPE_SELECT);
		WebElement isuburb = WebDriverUtils.findElementByCssSelector(driver,
				Constants.Selectors.AMITY_SUBURB_SELECT);
		WebElement ibeds = WebDriverUtils.findElementByCssSelector(driver,
				Constants.Selectors.AMITY_BEDS_SELECT);
		WebElement ibaths = WebDriverUtils.findElementByCssSelector(driver,
				Constants.Selectors.AMITY_BATHS_SELECT);
		WebElement icarparks = WebDriverUtils.findElementByCssSelector(driver,
				Constants.Selectors.AMITY_CARPARKS_SELECT);
		
		combo = new AmityCombination();

		clickOption(iproperty_type, property_type, PROPERTY_TYPE);
		clickOption(isuburb, suburb, SUBURB);
		clickOption(ibeds, beds, BEDS);
		clickOption(ibaths, baths, BATHS);
		clickOption(icarparks, carparks, CARPARKS);
		
		System.out.println(combo.getPropertyTypes());
		System.out.println(combo.getSuburb());
		System.out.println(combo.getBeds());
		System.out.println(combo.getBaths());
		System.out.println(combo.getCarparks());
		
		setCombo(combo);
	}

	private void clickOption(WebElement dropdown, String option, int variable) {
		System.out.println("Clicking Element: " + dropdown + " with option: "
				+ option);
		WebDriverUtils.scrollToElement(driver, dropdown,
				WebDriverUtils.BY_ACTIONS);
		dropdown.click();

		WebElement dropdown_option = WebDriverUtils.findElementByCssSelector(
				driver, option);
		switch (variable) {
			case PROPERTY_TYPE:
				combo.setPropertyTypes(dropdown_option.getText());
				break;
			case SUBURB:
				combo.setSuburb(dropdown_option.getText());
				break;
			case BEDS:
				combo.setBeds(dropdown_option.getText());
				break;
			case BATHS:
				combo.setBaths(dropdown_option.getText());
				break;
			case CARPARKS:
				combo.setCarparks(dropdown_option.getText());
				break;
		}
		System.out.println(dropdown_option.getText());
		WebDriverUtils.scrollToElement(driver, dropdown_option,
				WebDriverUtils.BY_JAVASCRIPT);
		dropdown_option.click();
	}

	public AmityCombination getCombo() {
		return combo;
	}

	public void setCombo(AmityCombination combo) {
		this.combo = combo;
	}
	
	
}
