package com.oxygenventures.amity.tests;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.oxygenventures.amity.objects.AmityCombination;
import com.oxygenventures.amity.objects.Result;
import com.oxygenventures.amity.objects.Test;
import com.oxygenventures.amity.objects.TestText;
import com.oxygenventures.amity.utils.AssertUtils;
import com.oxygenventures.amity.utils.Constants;
import com.oxygenventures.amity.utils.StringPattern;
import com.oxygenventures.amity.utils.WebDriverUtils;

public class SearchedContentsTest extends Test {
	private static final String TAG = SearchedContentsTest.class
			.getSimpleName();

	private WebElement search_result = null;
	private AmityCombination combo;
	private List<WebElement> search_rows;
	private String comparetext;
	private String result;
	private Result results;
	private List<String> list_of_results = new ArrayList<String>();
	private List<TestText> list_of_assertions = new ArrayList<TestText>();
	private TestText testtext;

	public SearchedContentsTest(WebDriver driver) {
		super(driver);
	}

	public SearchedContentsTest(WebDriver driver, AmityCombination combo) {
		super(driver);
		this.combo = combo;
	}

	@Override
	public void performTest() {
		performAction();
		try {
			for (WebElement element : search_rows) {
				assertElements(element, Constants.Selectors.AMITY_RESULT_ONE);
				assertElements(element, Constants.Selectors.AMITY_RESULT_TWO);
				assertElements(element, Constants.Selectors.AMITY_RESULT_THREE);
			}

			WebElement nextpage = WebDriverUtils.findElementByCssSelector(
					driver, Constants.Selectors.AMITY_NEXT_PAGE);
			if (nextpage != null) {
				WebDriverUtils.scrollToElement(driver, nextpage,
						WebDriverUtils.BY_JAVASCRIPT);
				nextpage.click();
				performTest();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void performAction() {
		try {
			WebElement searchpage = WebDriverUtils.findElementByXpath(driver,
					Constants.Selectors.AMITY_SEARCHED_ITEMS);
			search_rows = searchpage.findElements(By
					.xpath(Constants.Selectors.AMITY_SEARCH_WRAPPER));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void doAction() {
		try {
			WebElement searchpage = WebDriverUtils.findElementByXpath(driver,
					Constants.Selectors.AMITY_SEARCHED_ITEMS);
			search_rows = searchpage.findElements(By
					.xpath(Constants.Selectors.AMITY_SEARCH_WRAPPER));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String doTest() {
		performAction();
		list_of_assertions = new ArrayList<TestText>();
		try {
			for (WebElement element : search_rows) {
				list_of_results.add(assertElements(element,
						Constants.Selectors.AMITY_RESULT_ONE));
				list_of_results.add(assertElements(element,
						Constants.Selectors.AMITY_RESULT_TWO));
				list_of_results.add(assertElements(element,
						Constants.Selectors.AMITY_RESULT_THREE));
			}

			WebElement nextpage = WebDriverUtils.findElementByCssSelector(
					driver, Constants.Selectors.AMITY_NEXT_PAGE);
			if (nextpage != null) {
				WebDriverUtils.scrollToElement(driver, nextpage,
						WebDriverUtils.BY_JAVASCRIPT);
				nextpage.click();
				performTest();
			}

			for (int i = 0; i < list_of_results.size(); i++) {
				if (list_of_results.get(i).contains("FAIL")) {
					result = list_of_results.get(i);
				} else {
					result = list_of_results.get(i);
				}
			}

			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	private String compareText(String expected, String actual) {
		if (!expected.equals("Any")) {
			System.out.println("Checking Expected: " + expected
					+ " vs Actual: " + actual);
			result = AssertUtils.isTextEqual(TAG, expected, actual,
					"RESULT NOT THE SAME! Expected: " + expected + " Actual: "
							+ actual);
		} else {
			result = TAG + " -- PASS";
		}

		return result;
	}

	private String containsText(String expected, String actual) {
		if (!expected.equals("All Suburbs")) {
			System.out.println("Checking Expected: " + expected
					+ " vs Actual: " + actual);
			result = AssertUtils.isTrue(TAG, actual.contains(expected),
					"RESULT DOESN\'T CONTAIN EXPECTED VALUE! Expected: "
							+ expected + " Actual: " + actual);
		} else {
			result = TAG + " -- PASS";
		}

		return result;
	}

	private String parsePrice(String price) {
		String parser = price.replace("$", "");
		parser = parser.replace(",", "");
		return parser.substring(0, parser.indexOf("."));
	}

	private String comparePrice(String expected, String actual) {
		try {
			System.out.println("Checking Expected: " + expected
					+ " vs Actual: " + actual);
			int x = Integer.parseInt(expected);
			int y = Integer.parseInt(actual);
			return AssertUtils.isTrue(TAG, y <= x,
					"PRICE IS GREATER THAN EXPECTED! Expected: " + expected
							+ " Actual: " + actual);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "PASS";
	}

	private String assertElements(WebElement element, String constant) {
		try {
			search_result = element.findElement(By.xpath(constant));

			results = new Result();
			testtext = new TestText();

			comparetext = search_result.findElement(
					By.xpath(Constants.Selectors.AMITY_FIRST_CHOICE)).getText();
			result = compareText(combo.getBeds(), comparetext);
			System.out.println(result);
			results.setBeds(result);
			testtext.setActual(comparetext);
			testtext.setExpected(combo.getBeds());
			testtext.setResult(result);
			list_of_assertions.add(testtext);

			comparetext = search_result.findElement(
					By.xpath(Constants.Selectors.AMITY_SECOND_CHOICE))
					.getText();
			result = compareText(combo.getBaths(), comparetext);
			System.out.println(result);
			results.setBaths(result);
			testtext.setActual(comparetext);
			testtext.setExpected(combo.getBaths());
			testtext.setResult(result);
			list_of_assertions.add(testtext);

			comparetext = search_result.findElement(
					By.xpath(Constants.Selectors.AMITY_THIRD_CHOICE)).getText();
			result = compareText(combo.getCarparks(), comparetext);
			System.out.println(result);
			results.setCarparks(result);
			testtext.setActual(comparetext);
			testtext.setExpected(combo.getCarparks());
			testtext.setResult(result);
			list_of_assertions.add(testtext);

			comparetext = search_result.findElement(
					By.xpath(Constants.Selectors.AMITY_NAME)).getText();
			result = containsText(combo.getSuburb(), comparetext);
			System.out.println(result);
			results.setName(result);
			testtext.setActual(comparetext);
			testtext.setExpected(combo.getSuburb());
			testtext.setResult(result);
			list_of_assertions.add(testtext);

			System.out.println("the price: "+combo.getPrice());
			System.out.println(String.valueOf(!StringPattern.match(combo.getPrice(), "0*0")));
			if (combo.getPrice() != null && !StringPattern.match(combo.getPrice(), "0*0")) {
				comparetext = search_result.findElement(
						By.xpath(Constants.Selectors.AMITY_PRICE)).getText();
				if (!comparetext.equals("Contact Agent")) {
					comparetext = parsePrice(comparetext);
					result = comparePrice(combo.getPrice().substring(2),
							comparetext);
					System.out.println(result);
					results.setPrice(result);
					testtext.setActual(comparetext);
					testtext.setExpected(combo.getPrice().substring(2));
					testtext.setResult(result);
					list_of_assertions.add(testtext);
				} else {
					result = TAG + " -- PASS";
					results.setPrice(result);
				}
			}

			results.setFinalresultss(results.getName(), results.getBeds(),
					results.getBaths(), results.getCarparks(),
					results.getPrice());
			return results.getFinalresults();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

	public List<TestText> getListOfAssertions() {
		return list_of_assertions;
	}
}
