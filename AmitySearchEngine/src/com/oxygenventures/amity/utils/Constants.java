package com.oxygenventures.amity.utils;

public class Constants {
	public static final String AMITY_URL = "stg-amity.test360.com.au";
	public static final String AMITY_URL_NEW = "stg.amity.com.au";
	public static final String AMITY_USERNAME = "amity_viewer";
	public static final String AMITY_PASSWORD = "amity9394";
	public static final String WORDPRESS_USERNAME = "admin";
	public static final String WORDPRESS_PASSWORD = "amity123#";
	public static final int SHORT_TIMEOUT = 20;
	
	public static class Selectors {
		public static final String WORDPRESS_USERNAME_FIELD = "#user_login";
		public static final String WORDPRESS_PASSWORD_FIELD = "#user_pass";
		public static final String WORDPRESS_LOGIN_BUTTON = "#wp-submit";
		
		public static final String AMITY_LOGO = ".site-title > a:nth-child(1)";

		public static final String AMITY_PROPERTY_TYPE_SELECT = "#property-type";
		public static final String AMITY_SUBURB_SELECT = "#suburb";
		public static final String AMITY_BEDS_SELECT = "#beds";
		public static final String AMITY_BATHS_SELECT = "#baths";
		public static final String AMITY_CARPARKS_SELECT = "#garage";
		public static final String AMITY_SEARCH_BUTTON = "#searchsubmit";

		public static final String AMITY_PROPERTY_TYPE_ALL_OPTION = "#property-type > option:nth-child(1)";
		public static final String AMITY_PROPERTY_TYPE_UNIT_OPTION = "#property-type > option:nth-child(2)";
		public static final String AMITY_PROPERTY_TYPE_APARTMENT_OPTION = "#property-type > option:nth-child(3)";

		public static final String AMITY_SUBURB_ALL_OPTION = "#suburb > option:nth-child(1)";
		public static final String AMITY_SUBURB_CAMBERWELL_OPTION = "#suburb > option:nth-child(2)";
		public static final String AMITY_SUBURB_CARNEGIE_OPTION = "#suburb > option:nth-child(3)";
		public static final String AMITY_SUBURB_CARRUM_OPTION = "#suburb > option:nth-child(4)";
		public static final String AMITY_SUBURB_CHELSEA_OPTION = "#suburb > option:nth-child(5)";
		public static final String AMITY_SUBURB_DANDENONG_OPTION = "#suburb > option:nth-child(6)";
		public static final String AMITY_SUBURB_MENTONE_OPTION = "#suburb > option:nth-child(7)";
		public static final String AMITY_SUBURB_MURRUMBEENA_OPTION = "#suburb > option:nth-child(8)";
		public static final String AMITY_SUBURB_SOUTHBANK_OPTION = "#suburb > option:nth-child(9)";
		public static final String AMITY_SUBURB_ST_KILDA_OPTION = "#suburb > option:nth-child(10)";
		public static final String AMITY_SUBURB_WEST_FOOTSCRAY_OPTION = "#suburb > option:nth-child(11)";
//		public static final String AMITY_SUBURB_BENTLEIGH_OPTION = "#suburb > option:nth-child(12)";
//		public static final String AMITY_SUBURB_BRIGHTON_OPTION = "#suburb > option:nth-child(13)";
//		public static final String AMITY_SUBURB_CARNEGIE_2_OPTION = "#suburb > option:nth-child(14)";
//		public static final String AMITY_SUBURB_MELBOURNE_OPTION = "#suburb > option:nth-child(15)";
//		public static final String AMITY_SUBURB_PRAHRAN_OPTION = "#suburb > option:nth-child(16)";
//		public static final String AMITY_SUBURB_WEST_FOOTSCRAY_2_OPTION = "#suburb > option:nth-child(17)";

		public static final String AMITY_BEDS_ANY_OPTION = "#beds > option:nth-child(1)";
		public static final String AMITY_BEDS_TWO_OPTION = "#beds > option:nth-child(2)";
		public static final String AMITY_BEDS_ONE_OPTION = "#beds > option:nth-child(3)";

		public static final String AMITY_BATHS_ANY_OPTION = "#baths > option:nth-child(1)";
		public static final String AMITY_BATHS_TWO_OPTION = "#baths > option:nth-child(2)";
		public static final String AMITY_BATHS_ONE_OPTION = "#baths > option:nth-child(3)";

		public static final String AMITY_CARPARK_ANY_OPTION = "#garage > option:nth-child(1)";
		public static final String AMITY_CARPARK_ONE_OPTION = "#garage > option:nth-child(2)";

		public static final String AMITY_PRICE_START = "div.slider-wrap:nth-child(8) > div:nth-child(2) > span:nth-child(1) > span:nth-child(2) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(1) > div:nth-child(2)";
		public static final String AMITY_PRICE_SLIDER = "div.slider-wrap:nth-child(8) > div:nth-child(2) > span:nth-child(1) > span:nth-child(2) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(1) > div:nth-child(3)";
		public static final String AMITY_PRICE_END = "div.slider-wrap:nth-child(8) > div:nth-child(2) > span:nth-child(1) > span:nth-child(2) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(1) > div:nth-child(1) > i:nth-child(3)";
		public static final String AMITY_PRICE_GETTER = "#Slider1";
		
		public static final String[] AMITY_PROPERTY_TYPE_OPTIONS = {
				AMITY_PROPERTY_TYPE_ALL_OPTION,
				AMITY_PROPERTY_TYPE_UNIT_OPTION,
				AMITY_PROPERTY_TYPE_APARTMENT_OPTION };
		public static final String[] AMITY_SUBURB_OPTIONS = {
				AMITY_SUBURB_ALL_OPTION, AMITY_SUBURB_CAMBERWELL_OPTION,
				AMITY_SUBURB_CARNEGIE_OPTION, AMITY_SUBURB_CARRUM_OPTION,
				AMITY_SUBURB_CHELSEA_OPTION, AMITY_SUBURB_DANDENONG_OPTION,
				AMITY_SUBURB_MENTONE_OPTION, AMITY_SUBURB_MURRUMBEENA_OPTION,
				AMITY_SUBURB_SOUTHBANK_OPTION, AMITY_SUBURB_ST_KILDA_OPTION,
				AMITY_SUBURB_WEST_FOOTSCRAY_OPTION };
//				AMITY_SUBURB_WEST_FOOTSCRAY_OPTION,
//				AMITY_SUBURB_BENTLEIGH_OPTION, AMITY_SUBURB_BRIGHTON_OPTION,
//				AMITY_SUBURB_CARNEGIE_2_OPTION, AMITY_SUBURB_MELBOURNE_OPTION,
//				AMITY_SUBURB_PRAHRAN_OPTION,
//				AMITY_SUBURB_WEST_FOOTSCRAY_2_OPTION };
		public static final String[] AMITY_BEDS_OPTIONS = {
				AMITY_BEDS_ANY_OPTION, AMITY_BEDS_TWO_OPTION,
				AMITY_BEDS_ONE_OPTION };
		public static final String[] AMITY_BATHS_OPTIONS = {
				AMITY_BATHS_ANY_OPTION, AMITY_BATHS_TWO_OPTION,
				AMITY_BATHS_ONE_OPTION };
		public static final String[] AMITY_CARPARK_OPTIONS = {
				AMITY_CARPARK_ANY_OPTION, AMITY_CARPARK_ONE_OPTION };
		
		public static final String AMITY_SEARCHED_ITEMS = "//div[@class='content-sidebar-wrap']";
		public static final String AMITY_SEARCHED_ITEMS_CSS = ".content-sidebar-wrap";
		public static final String AMITY_SEARCH_WRAPPER = ".//div[@class='searches-wrapper']";
		
		public static final String AMITY_RESULT_ONE = ".//div[contains(@class, 'searchresultbox1')]";
		public static final String AMITY_RESULT_TWO = ".//div[contains(@class, 'searchresultbox2')]";
		public static final String AMITY_RESULT_THREE = ".//div[contains(@class, 'searchresultbox3')]";
		
		public static final String AMITY_FIRST_CHOICE = "(.//div[@class='searchinfo']//span)[1]";
		public static final String AMITY_SECOND_CHOICE = "(.//div[@class='searchinfo']//span)[2]";
		public static final String AMITY_THIRD_CHOICE = "(.//div[@class='searchinfo']//span)[3]";
		
		public static final String AMITY_NAME = ".//p[@class='p-searchname']";
		public static final String AMITY_PRICE = ".//div[@class='search-wrapp']//div[@class='searchprice']//span[@class='spn-search-price']//span[@class='spn-dollar-price']";
		
		public static final String AMITY_NEXT_PAGE = ".pagination-next > a:nth-child(1)";
	}
}
