package com.oxygenventures.amity.utils;

public class CalendarUtils {

	public static final int GOOGLE = 1;
	public static final int OUTLOOK_2007 = 2;
	public static final int OUTLOOK_DOT_COM = 3;
	public static final int IPHONE_IPAD = 4;
	public static final int ANDROID = 5;
	public static final int WINDOWS_PHONE = 6;
	public static final int OTHER = 7;
	
	public static final int MAC_APPLE_ICAL = 0;
	public static final int MAC_IPHONE_IPAD = 2;
	public static final int MAC_GOOGLE = 3;
	public static final int MAC_OUTLOOK_DOT_COM = 4;
	public static final int MAC_ANDROID = 5;
	public static final int MAC_WINDOWS_PHONE = 6;
	public static final int MAC_OTHER = 7;

	public static final class InstructionsText {
		public static final String ICAL_INSTRUCTIONS_TITLE = "Thanks for Subscribing";
		public static final String ICAL_INSTRUCTIONS_TEXT = "You should see a pop-up shortly. Simply click the *Launch* button\nand follow the prompts to add schedules to your calendar";
		public static final String ICAL_MORE_INFO = "Instructions\nClick the Subscribe button to launch iCal, and click 'Subscribe' to add this calendar and subscribe to updates.\nIf iCal doesn't launch, simply open iCal, click 'Calendar' * 'Subscribe'.\nAdd the 'Calendar Address' webcal://c.e-qo.de/*.ics and click 'Subscribe'.";
		
		public static final String GOOGLE_INSTRUCTIONS_TITLE = "Thanks for Subscribing";
		public static final String GOOGLE_INSTRUCTIONS_TEXT = "You should see a pop-up shortly. Simply click the *Launch* button\nand follow the prompts to add schedules to your calendar";
		public static final String GOOGLE_MORE_INFO = "Instructions\nOpen Google Calendar.\nIn the 'Other calendars' drop-down on the left, select 'Add by URL'\nEnter the 'Calendar Address' webcal://c.e-qo.de/*.ics in the field provided.\nClick 'Add Calendar' and subscribe to updates.\nTo sync with Android, open your Android Phone Calendar.\nClick on Menu > Settings > Your Account or Menu > Calendars to display. The menu items vary depending on the Android Version.\nSelect the Calendar you have just created to sync with the Android Phone.\n\nGoogle Calendar may take up to 24 hours to update the calendar subscription content.";

		public static final String OUTLOOK_2007_INSTRUCTIONS_TITLE = "Thanks for Subscribing";
		public static final String OUTLOOK_2007_INSTRUCTIONS_TEXT = "You should see a pop-up shortly. Simply click the *Launch* button\nand follow the prompts to add schedules to your calendar";
		public static final String OUTLOOK_2007_MORE_INFO = "Instructions\nClick the Subscribe button to launch MS Outlook, and click 'Yes' to add this calendar and subscribe to updates.\nIf MS Outlook doesn't launch automatically, simply copy and paste the 'Calendar Address' webcal://c.e-qo.de/*.ics below into Internet Explorer and click 'enter'.\nSelect 'MS Outlook' if Internet Explorer detects multiple calendar software and click 'OK'.\nClick 'Yes' to add this calendar and subscribe to updates.";

		public static final String OUTLOOK_DOT_COM_INSTRUCTIONS_TITLE = "Thanks for Subscribing";
		public static final String OUTLOOK_DOT_COM_INSTRUCTIONS_TEXT = "You should see a pop-up shortly. Simply click the *Launch* button\nand follow the prompts to add schedules to your calendar";
		public static final String OUTLOOK_DOT_COM_MORE_INFO = "Instructions*Open Windows Live Calendar.*Click 'Subscribe to a Public Calendar'.*Enter the 'Calendar Address' webcal://c.e-qo.de/*.ics.*Click 'Subscribe to Calendar' to add this calendar and subscribe to updates.";

		public static final String IPHONE_IPAD_INSTRUCTIONS_TITLE = "Thanks for Subscribing";
		public static final String IPHONE_IPAD_INSTRUCTIONS_TEXT = "You should see a pop-up shortly. Simply click the *Launch* button\nand follow the prompts to add schedules to your calendar";
		public static final String IPHONE_IPAD_MORE_INFO = "Send SMS\nSend the calendar address to your device via SMS. Once you receive the message on your device, touch on the Calendar Address to subscribe to the calendars.\nNote: Depending on your mobile plan, one SMS charge (at the standard rate) may apply to send this 'Calendar Address' to your iPhone (no additional charges apply).\n\nSend email\nSend the calendar address to your device via email. Once you receive the email message on your device, touch the Calendar Address to subscribe to the calendars.";

		public static final String ANDROID_INSTRUCTIONS_TITLE = "Thanks for Subscribing";
		public static final String ANDROID_INSTRUCTIONS_TEXT = "You should see a pop-up shortly. Simply click the *Launch* button\nand follow the prompts to add schedules to your calendar";
		public static final String ANDROID_MORE_INFO = "To subscribe to calendars in Android, simply sync your device with your Google Calendar.";

		public static final String WINDOWS_PHONE_INSTRUCTIONS_TITLE = "Thanks for Subscribing";
		public static final String WINDOWS_PHONE_INSTRUCTIONS_TEXT = "You should see a pop-up shortly. Simply click the *Launch* button\nand follow the prompts to add schedules to your calendar";
		public static final String WINDOWS_PHONE_MORE_INFO = "Instructions\nOpen Windows Live Calendar.\nClick 'Subscribe to a Public Calendar'.\nEnter the 'Calendar Address' webcal://c.e-qo.de/*.ics.\nClick 'Subscribe to Calendar' to add this calendar and subscribe to updates.";
		
		public static final String OTHERS_INSTRUCTIONS_TITLE = "Thanks for Subscribing";
		public static final String OTHERS_INSTRUCTIONS_TEXT = "To complete your subscription, choose the calendar \napplication you use and follow the instructions below";
		
//		public static final String OUTLOOK_OLDER_THAN_2007_INSTRUCTIONS = "Instructions\nSimply download and save the file to your computer (e.g: to your desktop).\nBefore opening the file, open MS Outlook Calendar and click 'File' > 'Import and Export' > 'Import an iCalendar (.ics) or vCalendar file (.vcs)'.\nBrowse to select the file, and proceed to import.";
		public static final String OUTLOOK_OLDER_THAN_2007_INSTRUCTIONS = "Instructions*Simply download and save the file to your computer (e.g: to your desktop).*Before opening the file, open MS Outlook Calendar and click 'File'*>*'Import and Export'*>*'Import an iCalendar (.ics) or vCalendar file (.vcs)'.*Browse to select the file, and proceed to import.";
		public static final String OUTLOOK_FOR_MAC_INSTRUCTIONS = "Instructions*Simply download and save the file (eg: in your Downloads folder).*In Outlook for Mac, at the bottom of the navigation pane, click Calendar.*In the Finder, go to your Downloads folder and locate the 'My_E_CAL' .ics file.*Simply drag the .ics file to the Outlook Calendar grid or list.";
		public static final String LOTUS_NOTES_INSTRUCTIONS = "Instructions*Simply download and save the file to your computer (e.g: to your desktop).*'Right-click' the file and click 'Import All'.*Appointments will appear as separate invites in your inbox.*Accept each individually for the events to appear in your calendar.";
		public static final String BLACKBERRY_INSTRUCTIONS = "Instructions*To subscribe to calendars in Blackberry, simply sync your device with your preferred calendar program such as MS Outlook, iCal etc.";
		public static final String AOL_INSTRUCTIONS = "Instructions*Go to AOL Keyword: Calendar or go to calendar.aol.com.*On the left panel, below the list of calendars, click 'Add Calendar'.*Click the 'Subscribe to a Calendar' link.*Enter the Calendar Address enter a calendar name, and click 'Subscribe' to add this calendar and subscribe to updates.";
		public static final String YAHOO_INSTRUCTIONS = "Instructions*Open Yahoo beta Calendar.*Click the '+' button next to 'Calendars' on the left of the page and click 'Subscribe to Calendar'.*Enter the 'Calendar Address' and click 'Next'.*Click 'Save' to add this calendar and subscribe to updates.";
		public static final String WINDOWS_VISTA_INSTRUCTIONS = "Instructions*Open Windows Vista Calendar.*Click 'Subscribe'.*Enter the 'Calendar Address' and follow the instructions provided to add this calendar and subscribe to updates.";
		
		public static final String[] OTHER_CALENDARS_INSTRUCTIONS_LIST = {OUTLOOK_OLDER_THAN_2007_INSTRUCTIONS, OUTLOOK_FOR_MAC_INSTRUCTIONS, LOTUS_NOTES_INSTRUCTIONS, BLACKBERRY_INSTRUCTIONS, AOL_INSTRUCTIONS, YAHOO_INSTRUCTIONS, WINDOWS_VISTA_INSTRUCTIONS};
	}
	
	public static final class WEBCAL_LINK {
		public static final String GOOGLE_URL = "google";
		public static final String OUTLOOK_DOT_COM = "live.com";
	}

}
