package com.oxygenventures.amity.utils;

public class TimeOutUtils {
	public static final int LONG_TIMEOUT = 60;
	public static final int SHORT_TIMEOUT = 15;

	public static void sleep(int seconds) {
		try {
			Thread.sleep(seconds*1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
