package com.oxygenventures.amity.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;

import com.oxygenventures.amity.objects.AmityCombination;
import com.oxygenventures.amity.objects.Result;
import com.oxygenventures.amity.objects.TestText;

public class FileUtils {

	private static Writer writer;

	public static void createReport() {
		try {
			File file = new File("reports/report.csv");
			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(file)));
			writer.write("Property Type, Suburb, Beds, Baths, Carparks, Price, Results\n");
			System.out.println("Report file created!");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void writeReportWithResults(AmityCombination combination, Result results) {
		try {
			File file = new File("reports/report.csv");
			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(file, true)));
			writer.append(combination.getPropertyTypes() + ","
					+ combination.getSuburb() + "," + combination.getBeds()
					+ "," + combination.getBaths() + ","
					+ combination.getCarparks() + "," + combination.getPrice()
					+ "," + results.getFinalresults() + "\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void writeReport(AmityCombination combination, String results, List<TestText> assertions) {
		try {
			File file = new File("reports/report.csv");
			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(file, true)));
			
			if (results == null) {
				results = "PASS -- No Results to check";
			}
			writer.append(combination.getPropertyTypes() + ","
					+ combination.getSuburb() + "," + combination.getBeds()
					+ "," + combination.getBaths() + ","
					+ combination.getCarparks() + "," + combination.getPrice().substring(2)
					+ "," + results + "\n");
			for (TestText text : assertions) {
				writer.append("expected,"+text.getExpected()+",actual,"+text.getActual()+","+text.getResult()+"\n");
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
