package com.oxygenventures.amity.utils;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AssertUtils {

	private static final boolean SHOW_FAILURE_LOGS = true;
	private static final boolean SHOW_SUCCESS_LOGS = true;
	private static String result = "";

	public static final void assertTrue(String tag, boolean condition,
			String messageIfFalse) {
		if (SHOW_FAILURE_LOGS) {
			Assert.assertTrue(tag + " -- " + messageIfFalse, condition);
		}
		if (SHOW_SUCCESS_LOGS) {
			setPassTestMessage(tag);
		}
	}

	public static final String isTrue(String tag, boolean condition,
			String messageIfFalse) {

		if (condition) {
			result = tag + " -- PASS";
		} else {
			result = messageIfFalse;
		}

		return result;
	}

	public static final String isTextEqual(String tag, String expectedText,
			String foundText, String messageIfFalse) {

		if (foundText.equals(expectedText)) {
			result = tag + " -- PASS";
		} else {
			result = messageIfFalse;
		}

		return result;
	}

	public static final void assertText(String tag, String expectedText,
			String foundText, String messageIfFalse) {
		if (SHOW_FAILURE_LOGS) {
			Assert.assertEquals(tag + " -- " + messageIfFalse, expectedText,
					foundText);
		}

		if (SHOW_SUCCESS_LOGS) {
			setPassTestMessage(tag);
		}
	}

	public static final void assertAttributeHasText(String tag,
			WebElement element, String expectedAttribute,
			String attributeExpectedText,
			String messageIfAttributeValueNotMatch,
			String messageIfAttributeNotFound) {
		String attributeValue = element.getAttribute(expectedAttribute);
		if (SHOW_FAILURE_LOGS) {
			Assert.assertNotNull(tag + " -- " + messageIfAttributeNotFound,
					attributeValue);
		}
		if (SHOW_SUCCESS_LOGS) {
			System.out.println(tag + " -- PASS (attribute found)");
		}

		if (SHOW_FAILURE_LOGS) {
			Assert.assertTrue(tag + " -- " + messageIfAttributeValueNotMatch,
					attributeValue.contains(attributeExpectedText));
		}
		if (SHOW_SUCCESS_LOGS) {
			System.out.println(tag + " -- PASS (attribute " + expectedAttribute
					+ " has value " + attributeExpectedText + ")");
		}

	}

	public static final void assertElementVisibility(WebDriver driver,
			By locator, String tag, String messageIfVisibilityNotExpected,
			boolean shouldBeVisible) {
		try {
			WebElement element = new WebDriverWait(driver, 15)
					.until(ExpectedConditions
							.visibilityOfElementLocated(locator));

			if (SHOW_FAILURE_LOGS) {
				Assert.assertNotNull(tag + " -- FAILED (element not found)",
						element);
				Assert.assertTrue(
						tag + " -- " + messageIfVisibilityNotExpected,
						element.isDisplayed() == shouldBeVisible);
			}
			if (SHOW_SUCCESS_LOGS) {
				System.out.println(tag + " -- PASSED (element is visible)");
				System.out.println(tag + " -- PASSED (element found)");
			}
		} catch (Exception e) {
		}
	}

	public static final void assertContainsText(String tag,
			String expectedText, String foundText, String messageIfFalse) {
		if (SHOW_FAILURE_LOGS) {
			Assert.assertTrue(tag + " -- " + messageIfFalse,
					foundText.contains(expectedText));
		}

		if (SHOW_SUCCESS_LOGS) {
			setPassTestMessage(tag);
		}
	}

	private static final void setPassTestMessage(String tag) {
		System.out.println(tag + " -- PASS");
	}
}
