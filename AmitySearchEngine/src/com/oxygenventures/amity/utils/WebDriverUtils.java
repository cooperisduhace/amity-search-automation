package com.oxygenventures.amity.utils;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebDriverUtils {

	public static final int BY_JAVASCRIPT = 1;
	public static final int BY_ACTIONS = 2;

	private static Actions action = null;
	private static JavascriptExecutor js = null;

	public static void scrollToElement(WebDriver driver, WebElement element,
			int method) {

		try {
			switch (method) {
			case BY_JAVASCRIPT:
				js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].scrollIntoView();", element);
				break;
			case BY_ACTIONS:
				action = new Actions(driver);
				action.moveToElement(element).click().perform();
				break;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static WebElement findElementById(WebDriver driver, String locator) {
		return new WebDriverWait(driver, Constants.SHORT_TIMEOUT)
				.until(ExpectedConditions.presenceOfElementLocated(By
						.id(locator)));
	}

	public static WebElement findElementByCssSelector(WebDriver driver,
			String locator) {
		try {
			return new WebDriverWait(driver, Constants.SHORT_TIMEOUT)
					.until(ExpectedConditions.presenceOfElementLocated(By
							.cssSelector(locator)));
		} catch (Exception e) {
			System.out.println("Element Not Found!");
		}

		return null;
	}

	public static WebElement findElementByXpath(WebDriver driver, String locator) {
		return new WebDriverWait(driver, Constants.SHORT_TIMEOUT)
				.until(ExpectedConditions.presenceOfElementLocated(By
						.xpath(locator)));
	}

	public static List<WebElement> findElementsByXpath(WebDriver driver,
			String locator) {
		return new WebDriverWait(driver, Constants.SHORT_TIMEOUT)
				.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By
						.xpath(locator)));
	}

	public static void changeAttribute(WebDriver driver, WebElement element,
			String attr, String value) {
		js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute(\'" + attr + "\', \'"
				+ value + "\')", element);
		System.out.println("New Attribute: " + element.getAttribute(attr));
	}

	public static void dragElementByOffset(WebDriver driver,
			WebElement element, int x, int y) {
		action = new Actions(driver);
		action.dragAndDropBy(element, x, y).perform();
	}

}
